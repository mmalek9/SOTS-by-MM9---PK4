﻿using Indeks_Elektroniczny.Enums;
using Indeks_Elektroniczny.HttpConnect;
using Indeks_Elektroniczny.Model;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Indeks_Elektroniczny
{
    public partial class RegisterWindow : Window
    {
        private HttpConnector httpConnector = new HttpConnector();
        private void NumberField(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        

        public RegisterWindow()
        {
            InitializeComponent();
            CoursesList.ItemsSource = Enum.GetValues(typeof(Courses.CoursesEn)).Cast<Courses.CoursesEn>();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private async void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            try {
                Student student = new Student(Name.Text,
                    Surname.Text,
                    int.Parse(AlbumNo.Text),
                    Login.Text,
                    Password.Password,
                    1,
                    CoursesList.SelectedIndex);

                await httpConnector.Register(student);

                MessageBox.Show("Registered. You can log in now.", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.Replace("\"", ""), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }
    }
}
