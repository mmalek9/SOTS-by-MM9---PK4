﻿using Indeks_Elektroniczny.HttpConnect;
using Indeks_Elektroniczny.Model;
using System;
using System.Windows;

namespace Indeks_Elektroniczny
{
    public partial class MainWindow : Window
    {

        private HttpConnector httpConnector = new HttpConnector();
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (StudentCheck.IsChecked == true)
                {
                    //Student student = await httpConnector.Login(new Student(LoginBox.Text, PasswordBox.Password));
                    //MessageBox.Show("Success", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                    StudentWindow sw = new StudentWindow();
                    sw.Show();
                    this.Close();
                }
                if(TeacherCheck.IsChecked == true)
                {
                    //Teacher teacher = await httpConnector.Login(new Teacher(LoginBox.Text, PasswordBox.Password));
                    TeacherWindow tw = new TeacherWindow();
                    tw.Show();
                    this.Close();
                    
                }
                else if(TeacherCheck.IsChecked == false && StudentCheck.IsChecked == false)
                {
                    MessageBox.Show("Nie wybrano typu użytkownika");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.Replace("\"", ""), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            RegisterWindow registerWindow = new RegisterWindow();
            registerWindow.Show();
            this.Close();
        }

        private void StudentCheck_Checked(object sender, RoutedEventArgs e)
        {
            TeacherCheck.IsChecked = false;
        }

        private void TeacherCheck_Checked(object sender, RoutedEventArgs e)
        {
            StudentCheck.IsChecked = false;
        }
    }
}
