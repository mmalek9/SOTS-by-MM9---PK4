﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indeks_Elektroniczny.Model
{
    public class Student
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int AlbumNo { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int ActualSemester { get; set; }
        public int CourseID { get; set; }

        public Student(string firstName, string lastName, int albumNo, string login, string password, int actualSemester, int courseID)
        {
            FirstName = firstName;
            LastName = lastName;
            AlbumNo = albumNo;
            Login = login;
            Password = password;
            ActualSemester = actualSemester;
            CourseID = courseID;
        }

        public Student(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}