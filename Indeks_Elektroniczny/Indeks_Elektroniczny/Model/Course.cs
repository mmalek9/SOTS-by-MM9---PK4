﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Indeks_Elektroniczny.Model
{
    public class Course
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}