﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Indeks_Elektroniczny.Model
{
    public class Teacher
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public Teacher(string firstName, string lastName, string login, string password)
        {
            FirstName = firstName;
            LastName = lastName;
            Login = login;
            Password = password;
        }

        public Teacher(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}