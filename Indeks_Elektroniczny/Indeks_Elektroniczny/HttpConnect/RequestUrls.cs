﻿namespace Indeks_Elektroniczny.HttpConnect
{
    internal class RequestUrls
    {
        private const string DASH = "/";
        public const string SERVER_URL = "http://localhost:50466/api/";

        public class STUDENT
        {
            public const string STUDENTS = "Students/";
            public const string REGISTER = STUDENTS + "Register";
            public const string LOGIN = STUDENTS + "Login";
        }
        public class TEACHER
        {
            public const string TEACHERS = "Teachers/";
            public const string REGISTER = TEACHERS + "Register";
            public const string LOGIN = TEACHERS + "Login";
        }

    }
}
