﻿using Indeks_Elektroniczny.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;


namespace Indeks_Elektroniczny.HttpConnect
{
    public class HttpConnector
    {
        private RestClient client = new RestClient(RequestUrls.SERVER_URL);

        public async Task<Student> GetStudentAsync(int id)
        {
            RestRequest request = new RestRequest(RequestUrls.STUDENT.STUDENTS + id);
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            IRestResponse response = await client.ExecuteGetTaskAsync(request, cancellationTokenSource.Token);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Student student = await Task.Run(() => JsonConvert.DeserializeObject<Student>(response.Content));
                return student;
            }
            else
            {
                throw new Exception("Error code " + response.ErrorException);
            }
        }

        public async Task<Student> Register(Student newStudent)
        {
            RestRequest request = new RestRequest(RequestUrls.STUDENT.REGISTER);
            request.AddJsonBody(newStudent);
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            IRestResponse response = await client.ExecutePostTaskAsync(request, cancellationTokenSource.Token);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Student responseStudent = await Task.Run(() => JsonConvert.DeserializeObject<Student>(response.Content));
                return responseStudent;
            }
            else
            {
                throw new Exception(response.Content);
            }
        }

        public async Task<Student> Login(Student student)
        {
            RestRequest request = new RestRequest(RequestUrls.STUDENT.LOGIN);
            request.AddJsonBody(student);
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            IRestResponse response = await client.ExecutePostTaskAsync(request, cancellationTokenSource.Token);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Student responseStudent = await Task.Run(() => JsonConvert.DeserializeObject<Student>(response.Content));
                return responseStudent;
            }
            else
            {
                throw new Exception(response.Content);
            }
        }

        public async Task<Teacher> Login(Teacher teacher)
        {
            RestRequest request = new RestRequest(RequestUrls.TEACHER.LOGIN);
            request.AddJsonBody(teacher);
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            IRestResponse response = await client.ExecutePostTaskAsync(request, cancellationTokenSource.Token);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Teacher responseTeacher = await Task.Run(() => JsonConvert.DeserializeObject<Teacher>(response.Content));
                return responseTeacher;
            }
            else
            {
                throw new Exception(response.Content);
            }

        }
    }
}
